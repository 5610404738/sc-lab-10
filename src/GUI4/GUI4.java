package GUI4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI4 extends JFrame{
	
	private JPanel panel,panel2;
	private JFrame frame;
	private JColorChooser colour;
	private JComboBox combo;
	String[] color = {"Red","Green","Blue"};
	
	public GUI4(){
		Build();
	}
	
	public void Build(){
		frame = new JFrame();
		panel = new JPanel();
		panel2 = new JPanel();
		combo = new JComboBox<>(color);
		
		setLayout(new BorderLayout());
		add(panel,BorderLayout.CENTER);
		add(panel2,BorderLayout.SOUTH);
		
		//panel1
		panel.setBackground(Color.white);
		
		//panel2
		panel2.add(combo,BorderLayout.CENTER);
		
		combo.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(combo.getSelectedItem().equals("Red")){
					panel.setBackground(Color.RED);
				}
				if(combo.getSelectedItem().equals("Green")){
					panel.setBackground(Color.GREEN);
				}
				if(combo.getSelectedItem().equals("Blue")){
					panel.setBackground(Color.BLUE);
				}
			}
	
		});
	}
}
