package GUI2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class GUI2 extends JFrame{
	private JRadioButton green,red,blue;
	private JPanel panel,panel2;
	private JFrame frame;
	private JColorChooser colour;
		
	public GUI2(){
		Build();
	}
	
	public void Build(){
		frame = new JFrame();
		panel = new JPanel();
		panel2 = new JPanel();
		red =  new JRadioButton("Red");
		green = new JRadioButton("Green");
		blue = new JRadioButton("Blue");
		
		setLayout(new BorderLayout());
		add(panel,BorderLayout.CENTER);
		add(panel2,BorderLayout.SOUTH);
		//panel1
		panel.setBackground(Color.white);
		
		//panel2
		panel2.add(green,BorderLayout.WEST);
		panel2.add(red,BorderLayout.CENTER);
		panel2.add(blue,BorderLayout.EAST);
		
		red.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				//panel1
				panel.setBackground(Color.red);
			}
	});
		
		green.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				//panel1
				panel.setBackground(Color.green);
			}
	});
		
		blue.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				//panel1
				panel.setBackground(Color.blue);
			}
	});
		
	}
	
	
} 
	

